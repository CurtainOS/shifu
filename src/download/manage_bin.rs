use crate::structs::Binfo;
use reqwest::StatusCode;

use std::fs::File;
use std::io::Read;
use std::process::exit;
use toml::Value;

pub fn check_if_bin_exists(name: &str) -> String {
    let response =
        reqwest::blocking::get("https://ftp.curtainos.org/repo/amd64/index.toml")
            .unwrap_or_else(|e| {
                eprintln!("Unknown error: {}", e);
                exit(1)
            });

    match response.status() {
        StatusCode::OK => {
            let index_script = response.text().unwrap();
            let toml_index = index_script
                .as_str()
                .parse::<Value>().unwrap();


            if toml_index.get(name) == None {
                return "nope, this binary does not exist".to_string();
            }
            println!("{:?}", toml_index.get(name));
            return toml_index[name].as_str().unwrap().to_string();
        }
        _e => {
            eprintln!("this repo is missing an index file");
            exit(1);
        }
    }
}

pub fn get_bin_info(toml_file: String) -> Binfo {
    let mut file = File::open(toml_file).unwrap_or_else(|_| {
        eprintln!("this package does not include a info file. Aborting");
        exit(1);
    });
    let mut content = String::new();
    file.read_to_string(&mut content).unwrap();
    let config: Binfo = toml::from_str(content.as_str()).unwrap_or_else(|_| {
        eprintln!("The toml was not valid please contact the builder");
        exit(1);
    });
    config
}
