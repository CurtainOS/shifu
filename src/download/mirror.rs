use std::time::{Duration, SystemTime};

use reqwest::blocking::Client;
use reqwest::header::CONNECTION;

//many thanks to the folks at https://github.com/nelsonjchen/speedtest-rs for inspiring me with the mirrors


#[derive(Debug)]
pub enum MirrorError {
    Reqwest(reqwest::Error),
    SystemTimeError(std::time::SystemTimeError),
}
impl From<reqwest::Error> for MirrorError {
    fn from(err: reqwest::Error) -> MirrorError {
        MirrorError::Reqwest(err)
    }
}
impl From<std::time::SystemTimeError> for MirrorError {
    fn from(err: std::time::SystemTimeError) -> MirrorError {
        MirrorError::SystemTimeError(err)
    }
}


pub fn get_fastest_mirror(mirrorlist: Vec<String>) -> Result<String,MirrorError> {
    let mut fastest_server = None;
    let client = Client::new();
    let mut fastest_latency = Duration::new(u64::MAX, 0);
    'server_loop: for url in mirrorlist {
    let mut latency_measurements = vec![];
    for _ in 0..3 {
        let start_time = SystemTime::now();
        let res = client
            .get(&url)
            .header(CONNECTION, "close").send();
        if res.is_err() {
            continue 'server_loop;
        }
        _ = res?.bytes()?.last();
        let latency_measurement = SystemTime::now().duration_since(start_time)?;
        println!(" {} ms", latency_measurement.as_millis());
        latency_measurements.push(latency_measurement);
    }
    let latency = latency_measurements
    .iter()
    .fold(Duration::new(0, 0), |a, &i| a + i)
    / ((latency_measurements.len() as u32) * 2);
    println!("Trip calculated to {} ms", latency.as_millis());

if latency < fastest_latency {
    fastest_server = Some(url);
    fastest_latency = latency;
}



    }
println!("{}", fastest_server.clone().unwrap());    
Ok(fastest_server.unwrap())
}