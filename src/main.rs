pub use std::{env::args, process::exit};

use configuration::*;
use operations::errors::not_enough_args;
use operations::*;
mod configuration;
mod operations;
mod database;
mod dependencies;
mod download;
use crate::database::read::list_all_installed_packages;
use crate::build::build;
use crate::clean::clean_all;
use crate::handle_input::handle_install_types;
use crate::sideload::sideload;
use crate::database::write::add_db_package_entry;
use crate::system_functions::check_if_root;
use crate::download::mirror::get_fastest_mirror;
/// Handle argument parsing
fn handle_args() {
    let mut args = args();

    args.next();

    match args
        .next()
        .unwrap_or_else(|| {

            not_enough_args();
            unreachable!()
        })
        .replace('-', "")
        .as_ref()
    {
        "install" | "i" => {
            if args.len() < 1 {
                not_enough_args()
            }
            check_if_root();
            let args: Vec<String> = std::env::args().skip(2).collect();
            handle_install_types(args)
        }
        "clean" | "c" => {
            clean_all().unwrap();
        }

        "build" | "b" => {
            if args.len() < 1 {
                not_enough_args()
            }
            check_if_root();

            for package in args {
                build(&package);
            }
        }
        "blah" => {
            get_fastest_mirror(vec!["https://gnu.org".to_owned(), "https://curtainos.org".to_owned(), "https://archlinux.org".to_owned()]).unwrap();
        }
        "list" | "l" => {
            list_all_installed_packages();
        }

        "sideload" | "s" => {
            if args.len() < 2 {
                not_enough_args()
            }
            check_if_root();
            let args: Vec<String> = std::env::args().skip(2).collect();

            sideload(args);
        }

        "help" | "h" => {
            if args.len() == 1 {
                // help for individual arguments
                // print second argument

                match args.last().as_ref().unwrap().replace('-', "").as_str() {
                    "install" | "i" => {
                        println!("shifu install <packagename>");
                    }
                    "build" | "b" => {
                        println!("shifu build <packagename>");
                    }
                    "sideload" | "s" => {
                        println!("shifu sideload <packagename> <sideload_dir>");
                    }
                    _ => {
                        println!("unknown operation");
                        println!("shifu help <operation>");
                    }
                }
           } else if args.len() == 0 { 
            println!(
                r"shifu <OPERATION> [ARGUMENTS...]

    help     (h): Show this help message
    install  (i): Install one or more packages
    build    (b): Creat binaries for a given package
    sideload (s): Install binaries in a given directory
    clean    (c): Cleans all of shifus remaining files

")

            } else if args.len() > 1 {
                println!("Too many arguments");
            }

        }

        v => {
            println!("Operation not recognized: {}\nTry using 'shifu help'", v);
            exit(1)
        }
    }
}
// shifu man page
fn main() {
    handle_args()
}
