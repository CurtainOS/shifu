use colored::Colorize;
use crate::questions::yes_or_no;
use std::process::exit;
use crate::database::read::check_if_installed;
use question::Answer;


//we accept Option here because it makes life easier for some functions and avoids lots of boilerplate
pub fn check_and_ask(packages: Option<Vec<String>>) {
if packages != None && std::path::Path::new("/etc/shifu/shifu.db").exists() {
    let mut missing_packages = false;
    let mut missing_vec = Vec::new();
    for i in packages.clone().unwrap() {
        if !check_if_installed(i.clone()) && i != "libc" && i != "cc" { //cc and libc are legacy and no real packages. Old binaries may still have them as dependencies
            missing_packages = true;
            missing_vec.push(i.clone());
        }
    }
    if missing_packages {
        println!("You are missing packages!");
        for i in missing_vec {
            println!("{}", i.red());
        }
        if !yes_or_no("Are you Sure you want to continue?", Answer::NO) {
            eprintln!("See you soon");
            exit(1);
        }
    }
}}