use serde_derive::{Deserialize, Serialize};

//Build file
//
//
#[derive(Deserialize, Default)]
pub struct HandleBuildfile {
    pub package: Package,
    pub dependencies: Dependencies,
    pub install: Install,
}

#[derive(Deserialize, Default)]
pub struct Package {
    pub name: String,
    pub ver: String,
    pub des: String,
    pub stripping: bool,
}

#[derive(Deserialize, Default)]
pub struct Dependencies {
    pub build: Option<Vec<String>>,
    pub depends: Option<Vec<String>>,
    pub source: String,
    pub opt: Option<Vec<String>>,
    pub conflicts: Option<Vec<String>>,
}

#[derive(Deserialize, Default)]
pub struct Install {
    pub installation: String,
    pub sysvinit: Option<String>,
}


//binary info
//
//
#[derive(Deserialize, Serialize) ]
pub struct Binfo {
    pub info: Info,
    pub dependencies: BinDependencies,
}

#[derive(Deserialize, Serialize)]
pub struct Info {
    pub name: String,
    pub version: String,
    pub description: String,
}
#[derive(Deserialize, Serialize)]
pub struct BinDependencies {
    pub needed: Option<Vec<String>>,
    pub optional: Option<Vec<String>>,
}
// ____        _        _                    
// |  _ \  __ _| |_ __ _| |__   __ _ ___  ___ 
// | | | |/ _` | __/ _` | '_ \ / _` / __|/ _ \
// | |_| | (_| | || (_| | |_) | (_| \__ \  __/
// |____/ \__,_|\__\__,_|_.__/ \__,_|___/\___|
                                           
#[derive(Debug)]
pub struct DatabaseEntry {
    pub id:          i32,
    pub name:        String,
    pub installtype: String,
    pub version:     String, //has to be string because some packages have wierd version names
    pub description: String,
    pub depends:     Option<Vec<String>>,
}