use walkdir::WalkDir;
use is_executable::IsExecutable;
use std::process::Command;

pub fn strip_dir(directory: &String, should_strip: bool)  {
    if should_strip {
    print!("stripping");

    let walk_iter = WalkDir::new(directory).follow_links(true).into_iter();

    for entry_result in walk_iter {
        //println!("{}", entry.path().display());

        match entry_result {
            Ok(entry) => {
                //the first if checks if the file exists, some bug in walkdir shows nonexistent files
                if entry.path().exists() &&  entry.metadata().unwrap().is_file() && entry.path().is_executable() {
                    Command::new("strip").arg("--strip-debug").arg(&entry.path()).spawn().unwrap();
                    }
            }
            Err(e) => {
                eprintln!("{:?}", e);
                continue;
            }
        };
    }


}}