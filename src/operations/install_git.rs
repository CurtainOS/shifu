use crate::download::download::get_buildfile_or_error;
use crate::download::manage_git::clone;
use crate::add_db_package_entry;
use toml::Value;
use std::fs::File;
use std::io::prelude::*;
use crate::run::*;
use crate::structs::HandleBuildfile;
use crate::dependencies::simple::check_and_ask;


pub fn git_install(name: &str) {
     //here we parse the config file
     let mut file = File::open("/etc/shifu/config.toml").expect("could not open config file");
     let mut content = String::new();
      file.read_to_string(&mut content)
     .expect("Unable to read config file");
     let settings = content.parse::<Value>().unwrap();
     //end parsing config file
    let startroot = format!("/etc/shifu/cache/git/{}", name);
    let script = get_buildfile_or_error(format!("{}-git", name).as_str());
    let buildfile = mutate_buildfile(&script, settings);

    let config: HandleBuildfile = toml::from_str(buildfile.as_str()).expect("toml issue");
    clone(config.dependencies.source, &startroot);

    check_and_ask(config.dependencies.build.clone());
    check_and_ask(config.dependencies.depends.clone());


    let buildfile = [
        config.install.installation,
        config.install.sysvinit.unwrap_or_default(),
    ]
    .join("\n");

    run(buildfile, format!("/etc/shifu/cache/git/{}", name));
    let output = std::process::Command::new("git")
                    .arg("show").arg("--format=\"%h\"")
                     .arg("--no-patch")
                     .output().unwrap();
    let commit_hash = String::from_utf8(output.stdout).unwrap();
    println!("{}", commit_hash);
    add_db_package_entry(config.package.name, commit_hash, "git".to_string(), config.package.des, config.dependencies.depends);

}
