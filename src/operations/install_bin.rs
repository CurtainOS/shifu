use crate::archive::untar;
use crate::download::manage_bin::get_bin_info;
use crate::clean::clean;
use crate::dependencies::simple::check_and_ask;
use crate::compression::copy_file_to_not_mox;
use crate::download::download::async_download;
use crate::download::manage_bin::check_if_bin_exists;

use crate::database::write::add_db_package_entry;

use std::fs::remove_file;

use walkdir::WalkDir;


pub fn bin_install(name_list: Vec<String>, path: &str) {
let mut url_list = Vec::new();
let mut version_list = Vec::new();

for i in name_list.clone() {
    let bin_version = check_if_bin_exists(&i);
    version_list.push(bin_version.clone());
    let url = "https://ftp.curtainos.org/repo/amd64/".to_owned() + &i + "-" + &bin_version + ".curtain.mox";
    url_list.push(url);

}

    async_download(url_list, name_list.clone(), &"/etc/shifu/cache/bin/".to_owned());


for (name, version) in name_list.iter().zip(version_list) {
  
let moxfile = format!(
        "/etc/shifu/cache/bin/{}/{}-{}.curtain.mox",
        name, name, version
    );
//create a dir in the cache to avoid the next command erroring
std::fs::create_dir_all(std::path::Path::new(&format!("/etc/shifu/cache/bin/{}", name))).unwrap();
    //move the files downloaded by async hash 
std::fs::rename(format!("/etc/shifu/cache/bin/{}-{}.curtain.mox", name, version), moxfile.clone()).unwrap();

    let curtainfile = format!(
        "/etc/shifu/cache/bin/{}/{}-{}.curtain",
        name, name, version
    );

    copy_file_to_not_mox(moxfile.clone());
    untar(
        &curtainfile,
        format!("/etc/shifu/cache/bin/{}/{}-{}", name, name, version),
    );
    std::fs::remove_file(moxfile).unwrap();
    std::fs::remove_file(curtainfile).unwrap();

    let bindir = format!("/etc/shifu/cache/bin/{}/{}-{}", name, name, version);


    
    let info = get_bin_info(format!("{}/info.toml", bindir));





    check_and_ask(info.dependencies.needed.clone());



    //let mut from_paths = Vec::new();
    for entry in WalkDir::new(&bindir).min_depth(1).max_depth(1) {
        //parse through the fiorst files or folders in the dir
        let entry = entry.unwrap();
        if entry.path().exists() {
            std::process::Command::new("cp").arg("--remove-destination").arg("-r").arg(format!("{}", entry.path().display())).arg(&path).spawn().unwrap().wait().unwrap();


        }
       // from_paths.push(format!("{}", entry.path().display())); //append these to our "to copy" list
    }

  //remove hash.txt and info.toml_str
    remove_file("/info.toml").unwrap_or_else(|_| {
        println!("This binary did not come with a info.toml file");
    });
    remove_file("/hash.txt").unwrap_or_else(|_| {
        println!("This binary did not come with a hash.txt file");
    });
    add_db_package_entry(info.info.name, info.info.version, "bin".to_string(), info.info.description, info.dependencies.needed);

    println!("Succesfully installed {}🥳 Have a great day🥂", name);
    clean(name, "bin").unwrap();

}
}