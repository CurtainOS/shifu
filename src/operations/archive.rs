use futures::stream;
use futures::StreamExt;
use std::env;
use std::fs::{remove_dir_all, File};
use std::process::exit;
use std::process::Command;
use tar::Archive;
use tar::Builder;

pub fn create_tarball(name: String, ver: String, content: &String) {
    println!("{}     {}", name, content);
    let path = env::current_dir().unwrap();
    println!("The current directory is {}", path.display());
    let namver = format!("{}-{}", name, ver);
    let move_to_this_dir = Command::new("cp")
        .args(&[
            format!("/etc/shifu/cache/tmp/{}/to/{}", name, namver),
            namver.to_string(),
            "-r".to_string(),
        ])
        .spawn();

    match move_to_this_dir {
        Err(err) => {
            eprintln!("{}", err);
            exit(1);
        }
        Ok(mut child) => {
            child.wait().expect("failed to run ");
        }
    }
    let file = File::create(format!("{}.curtain", &namver)).unwrap();
    let mut tarball = Builder::new(file);
    Command::new("ls").spawn().unwrap().wait().unwrap();
    tarball.follow_symlinks(false);
    tarball.append_dir_all(&namver, &namver).unwrap(); //this is where somethinmg weird happened and idk why but from the small dir a huge tar gets created
    tarball.finish().unwrap();
    remove_dir_all(&namver).unwrap();
    println!("created tarball");
}
pub fn untar(path: &str, output: String) {
    //create all parent dirs of output
    let destination = std::path::Path::new(&output);
    std::fs::create_dir_all(destination).unwrap();
    println!("{}", path);
    let file = File::open(path).unwrap();
    let mut tar = Archive::new(file);
    tar.set_preserve_permissions(true);
    let dir_to_put = std::path::Path::new(&output).parent().unwrap();
    tar.unpack(dir_to_put).unwrap();
}

#[tokio::main]

pub async fn async_untar(files: &Vec<String>, destdir: &String) {
    let number_of_files = files.len();
    let concurrent_reqwests: usize = number_of_files;

    let bodies = stream::iter(files)
        .map(|outputname| {
            let destdir = &destdir;
            async move {
            let inputname = outputname.to_owned() + ".curtain";

            let input = std::path::Path::new(&inputname);
            println!("{}", inputname);
            //    let destination = filename;
            let file = File::open(input).unwrap();
            let mut tar = Archive::new(file);
            tar.set_preserve_permissions(true);


            tar.unpack(destdir).unwrap();
    }    })
        .buffer_unordered(concurrent_reqwests);

    bodies
        .for_each(|_f| async move {
            //join future
    }    )
        .await;
}
