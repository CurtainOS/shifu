use crate::download::download::download;
use crate::download::manage_git::clone;
use crate::database::write::add_db_package_entry;
use crate::run::*;
use toml::Value;
use std::io::prelude::*;
use crate::clean::clean;
use std::fs::File;
use crate::structs::HandleBuildfile;
use crate::dependencies::simple::check_and_ask;


pub fn source_install(config: HandleBuildfile, install_script: String) {
    println!(
        "Welcome, I will now install {} for you",
        config.package.name
    );

    check_and_ask(config.dependencies.build.clone());
    check_and_ask(config.dependencies.depends.clone());

    
    //here we parse the config file
    let mut file = File::open("/etc/shifu/config.toml").expect("could not open config file");
    let mut content = String::new();
     file.read_to_string(&mut content)
    .expect("Unable to read config file");
    let settings = content.parse::<Value>().unwrap();
    //end parsing config file

    let applied_script = mutate_buildfile(&install_script, settings);
    print!("{}", applied_script);

    let config: HandleBuildfile = toml::from_str(&applied_script).unwrap();

    let destdir = format!("/etc/shifu/cache/source/{}", config.package.name);

    let buildfile = [
        config.install.installation,
        config.install.sysvinit.unwrap_or_default(),
    ]
    .join("\n");
    if !config.dependencies.source.starts_with("git+") {
    download(
        &config.dependencies.source,
        format!("{}/{}.tar", destdir, config.package.name).as_str(),
    );
    std::process::Command::new("tar")
        .arg("-xf")
        .arg(format!("{}/{}.tar", destdir, config.package.name))
        .arg("-C")
        .arg(&destdir)
        .spawn()
        .unwrap()
        .wait()
        .unwrap();
} else {
    let git_repo = config.dependencies.source.replace("git+", "");
    std::fs::create_dir_all(&destdir).unwrap();
    clone(git_repo, &(destdir.clone() + "/" + &config.package.name + "-" + &config.package.ver));
    
}
    run(
        buildfile,
        format!("{}/{}-{}", destdir, config.package.name, config.package.ver),
    );
    clean(&config.package.name, "source").unwrap();
    add_db_package_entry(config.package.name, config.package.ver, "source".to_string(), config.package.des, config.dependencies.depends);
}
