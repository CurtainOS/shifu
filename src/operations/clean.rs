use std::fs::remove_dir_all;
use std::io::Error;
pub fn clean(pkgname: &str, installtype: &str) -> Result<(), String>{
    match installtype {
        "source" => clean_source(pkgname).unwrap(),
        "bin" => clean_bin(pkgname).unwrap(),
        "git" => clean_git(pkgname).unwrap(),
        "built" => clean_built_pkg(pkgname).unwrap(),
        _     => return Err("Invalid installtype was specified".to_string()),
    }
    Ok(())
}
fn clean_source(pkgname: &str) -> Result<(), Error>{
    let to_remove = "/etc/shifu/cache/source/".to_owned() + pkgname;
    remove_dir_all(to_remove)?;
    Ok(())
}
fn clean_bin(pkgname: &str) -> Result<(), Error>{
    let to_remove = "/etc/shifu/cache/bin/".to_owned() + pkgname;
    remove_dir_all(to_remove)?;
    Ok(())
}
fn clean_git(pkgname: &str) -> Result<(), Error>{
    let to_remove = "/etc/shifu/cache/git/".to_owned() + pkgname;
    remove_dir_all(to_remove)?;
    Ok(())
}
fn clean_built_pkg(pkgname: &str) -> Result<(), Error>{
    let to_remove = "/etc/shifu/cache/tmp/".to_owned() + pkgname;
    remove_dir_all(to_remove)?;
    Ok(())
}
pub fn clean_all() -> Result<(), Error> {
    //first block for source
    if  std::path::Path::new("/etc/shifu/cache/source/").exists() {
    let paths = std::fs::read_dir("/etc/shifu/cache/source/").unwrap();
    for file in paths {
        let to_remove: String = file.unwrap().path().display().to_string();
        clean_source(to_remove.replace("/etc/shifu/cache/source/", "").as_str()).unwrap();
    }}
    if  std::path::Path::new("/etc/shifu/cache/bin/").exists() {
    let paths = std::fs::read_dir("/etc/shifu/cache/bin/").unwrap();
    for file in paths {
        let to_remove: String = file.unwrap().path().display().to_string();
        clean_bin(to_remove.replace("/etc/shifu/cache/bin/", "").as_str()).unwrap();
    }}
    if  std::path::Path::new("/etc/shifu/cache/git/").exists() {
    let paths = std::fs::read_dir("/etc/shifu/cache/git/").unwrap();
    for file in paths {
        let to_remove: String = file.unwrap().path().display().to_string();
        clean_git(to_remove.replace("/etc/shifu/cache/git/", "").as_str()).unwrap();
    }}
    if  std::path::Path::new("/etc/shifu/cache/tmp/").exists() {
    let paths = std::fs::read_dir("/etc/shifu/cache/tmp/").unwrap();
    for file in paths {
        let to_remove: String = file.unwrap().path().display().to_string();
        clean_built_pkg(to_remove.replace("/etc/shifu/cache/tmp/", "").as_str()).unwrap();
    }}


    Ok(())
}