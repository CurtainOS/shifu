use crate::archive::create_tarball;
use crate::binfo::create_binary_info;
use crate::compression::copy_file_to_mox;
use crate::destdir::destdir;
use crate::download::download::{download, get_buildfile_or_error};
use crate::run::*;
use crate::strip::strip_dir;
use crate::clean::clean;
use crate::structs::HandleBuildfile;
use crate::dependencies::simple::check_and_ask;


use regex::Regex;
use std::fs;
use toml::Value;
use std::io::prelude::*;
use std::fs::File;
use std::io::Write;
use std::process::Command;

pub fn build(package_name: &str) {
    let install_script = get_buildfile_or_error(package_name);
    println!("{}", install_script);
    let toml_str = install_script.as_str();
    let config: HandleBuildfile = toml::from_str(toml_str).unwrap();

  
    check_and_ask(config.dependencies.build.clone());
    check_and_ask(config.dependencies.depends.clone());
    check_and_ask(config.dependencies.opt.clone());

    
    // this regex is used to allocate all "/" that do not have any text before them
    //for example /usr/bin could become test/usr/bin
       //here we parse the config file
       let mut file = File::open("/etc/shifu/config.toml").expect("could not open config file");
       let mut content = String::new();
        file.read_to_string(&mut content)
       .expect("Unable to read config file");
       let settings = content.parse::<Value>().unwrap();
       //end parsing config file

    regex::escape("/");
    let first_slash_regex = Regex::new(r#"(\s)/"#).unwrap();
    let build_name = format!("{}-{}", config.package.name, config.package.ver);
    println!("{}", build_name);
    let build_dir = format!(
        "/etc/shifu/cache/tmp/{}/to/{}",
        config.package.name, build_name
    );

    //apply said regex
    let fixed_to_right_dir = first_slash_regex
        .replace_all(&install_script, format!(" {}/", build_dir))
        .to_string();
    println!("{}", fixed_to_right_dir);

    //create basic filesystem to avoid errors when a package uses cp
    fs::create_dir_all(build_dir.as_str()).unwrap();
    //these command make a basic hierarchy to make builds work
    std::process::Command::new("mkdir").arg("-pv").arg(format!("{}/etc", build_dir)).arg(format!("{}/var", build_dir)).arg(format!("{}/usr/bin", build_dir)).arg(format!("{}/usr/sbin", build_dir)).arg(format!("{}/usr/lib", build_dir)).spawn().unwrap();
    std::process::Command::new("ln").arg("-sf").arg("usr/bin").arg(format!("{}/bin", build_dir)).spawn().unwrap();
    std::process::Command::new("ln").arg("-sf").arg("usr/lib").arg(format!("{}/lib", build_dir)).spawn().unwrap();
    std::process::Command::new("ln").arg("-sf").arg("usr/sbin").arg(format!("{}/sbin", build_dir)).spawn().unwrap();

    let edited = destdir(&fixed_to_right_dir, &build_dir);
    let finished = mutate_buildfile(&edited, settings);
    //temporarily dissabled because of interference with the ninja install
    // let finished1 = regex_replace(&replaced_cp, "(?m)^install(\\s)", "install -d ");

    //make a toml out of it to pass right build args
    let fincfg: HandleBuildfile = toml::from_str(&finished).unwrap();

    let buildfile = [
        fincfg.install.installation,
        fincfg.install.sysvinit.unwrap_or_default(),
    ]
    .join("\n");

    let downdir = format!("/etc/shifu/cache/tmp/{}/from", config.package.name);
    download(
        &fincfg.dependencies.source,
        &format!("{}/{}.tar", downdir, config.package.name),
    );
    println!("{}", buildfile);
    Command::new("tar")
        .arg("-xf")
        .arg(format!("{}/{}.tar", downdir, config.package.name))
        .arg("-C")
        .arg(&downdir)
        .spawn()
        .unwrap()
        .wait()
        .unwrap();

    let path = std::env::current_dir().unwrap();
    run(
        buildfile,
        //downdir,
        format!("{}/{}-{}", downdir, config.package.name, config.package.ver),
    );
    std::env::set_current_dir(path).unwrap();
    let info_info = create_binary_info(toml_str.to_string());
    let mut info_file = File::create(format!("{}/info.toml", &build_dir)).unwrap();
    info_file.write_all(info_info.as_bytes()).unwrap();

    // strip debug symbols
    strip_dir(&build_dir, config.package.stripping);

    
    create_tarball(
        fincfg.package.name.to_string(),
        fincfg.package.ver,
        &build_dir,
    );
    let filename = format!("{}.curtain", build_name);
    copy_file_to_mox(&filename);
    fs::remove_file(filename).unwrap();
    clean(&config.package.name, "built").unwrap();
}
