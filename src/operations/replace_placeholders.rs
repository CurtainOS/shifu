use crate::structs::HandleBuildfile;
use toml;
pub fn replace_placeholders(input: &str) -> String {
    let config: HandleBuildfile = toml::from_str(input).expect("toml issue");

    // we must do %source% first because if %source% contains for example %ver%, %ver% will else end up in the main code

    let result = str::replace(input, "%source%", &config.dependencies.source);
    let result2 = str::replace(&result, "%ver%", &config.package.ver);
    str::replace(&result2, "%name%", &config.package.name)
    // we must do %source% first because if %source% contains for example %ver%, %ver% will else end up in the main code
}
