use crate::download::download::get_buildfile;
use crate::errors::invalid_toml_format;
use crate::install_bin::bin_install;
use crate::install_git::git_install;
use crate::install_source::source_install;
use crate::manage::check_if_toml_exists;
use crate::download::manage_bin::check_if_bin_exists;
use crate::structs::HandleBuildfile;
use colored::Colorize;
use crate::questions::choose_from_multiple;
use toml::Value;
use std::fs::File;
use std::io::Read;
use crate::query::handle_query;
use std::process::exit;

//if user inputs a bin specifically
pub fn handle_install_types(packages: Vec<String>) {
           //here we parse the config file
           let mut file = File::open("/etc/shifu/config.toml").expect("could not open config file");
           let mut content = String::new();
            file.read_to_string(&mut content)
           .expect("Unable to read config file");
           let settings = content.parse::<Value>().unwrap();
           //end parsing config file
       

    //Generate vectors for the install type
    let mut to_bin_install = Vec::new();
    let mut to_git_install = Vec::new();
    let mut to_source_install = Vec::new();
    let mut to_local_source_install = Vec::new();


    let final_packages = handle_query(packages, settings);
    println!("{:?}", final_packages);
    for package_name in final_packages {
        if package_name.ends_with("-bin") {
            let mut nombre: String = package_name.to_string();
            for i in 1..5 {
                //prints a countdown
                println!("{}", 4 - i);
                //removed the -bin part
                nombre.pop();
            }
            //this gets version or a "nope, this...§ because we dont wanna use unwrap lul"
            let bincheck = check_if_bin_exists(&nombre);

            if &bincheck == "nope, this binary does not exist" {
                eprintln!("binary {} not found", nombre);
                exit(1);
            } else {
                to_bin_install.push(nombre.clone());
                //bin_install(&nombre, &bincheck, "/");
                continue;
            }
        } else if package_name.ends_with("-git") {
            let mut nombre: String = package_name.to_string();
            for i in 1..5 {
                //prints a countdown
                println!("{}", 4 - i);
                //removed the -git part
                nombre.pop();
            }

            if check_if_toml_exists(format!("{}-git", nombre).as_str()) {
                to_git_install.push(nombre.clone());
                //git_install(&nombre);
                continue;
            }
        } else if package_name.ends_with(".toml"){
            to_local_source_install.push(package_name);
        }else if package_name.ends_with("-source") {
            let mut nombre: String = package_name.to_string();
            for i in 1..8 {
                //prints a countdown
                println!("{}", 7 - i);
                //removed the -source part
                nombre.pop();
            }

            let script = get_buildfile(&nombre);
            if !script.trim().is_empty() {
                let config: HandleBuildfile =
                    toml::from_str(script.as_str()).unwrap_or_else(|_| {
                        invalid_toml_format();
                        exit(1);
                    });
                    to_source_install.push(nombre.clone());
               //source_install(config, script);
                continue;
            }
        } else {
            match ask_for_install_type(package_name.clone()).as_str() {
                "git" => to_git_install.push(package_name.clone()),
                "source" => to_source_install.push(package_name.clone()),
                "binary" => to_bin_install.push(package_name.clone()),
                _ => eprint!("returned none of the above"),
            }
            //install(&package_name);
        }
    }
    if !to_bin_install.is_empty() {
    bin_install(to_bin_install, "/");
    }
    for i in to_source_install {

        let script = get_buildfile(&i);
            if !script.trim().is_empty() {
                let config: HandleBuildfile =
                    toml::from_str(script.as_str()).unwrap_or_else(|_| {
                        invalid_toml_format();
                        exit(1);
                    });
               source_install(config, script);
                continue;
            }
    }

    for i in to_git_install {
        git_install(&i);
    }
    for i in to_local_source_install {
        let local_file = std::fs::read_to_string(i).unwrap();
        let config: HandleBuildfile =
                    toml::from_str(local_file.as_str()).unwrap_or_else(|_| {
                        invalid_toml_format();
                        exit(1);
                    });
        source_install(config, local_file);
    }

}

pub fn ask_for_install_type(package_name: String) -> String{
      //start the function if just the package name was presented
      let install_script = get_buildfile(package_name.as_str());
      let mut config: HandleBuildfile = Default::default();
      //predeclare the config, because it could stay unused
      let mut availible_versions = Vec::new();
  
      if !install_script.trim().is_empty() {
          print!("{}", install_script);
          config =
              toml::from_str(install_script.as_str()).unwrap_or_else(|_| {
                  invalid_toml_format();
                  exit(1);
              });
  
          println!(
              "Package {} found with ver: {}",
              package_name.red(),
              config.package.ver.green()
          );
          availible_versions.push("source");
      }
      let bincheck = check_if_bin_exists(package_name.as_str());
  
      if bincheck == "nope, this binary does not exist" {
          println!("bin not found")
      } else {
          println!(
              "Binary {} found with ver: {}",
              package_name.red(),
              bincheck.green()
          );
          availible_versions.push("binary");
      }
  
      if !check_if_toml_exists(format!("{}-git", package_name).as_str()) {
          println!("git version not found")
      } else {
          println!(
              "package {} found with ver: {}",
              package_name.red(),
              "git".green()
          );
          availible_versions.push("git");
      }
  
      let wanted_version = choose_from_multiple(
          availible_versions,
          "which version do you want to install? choose helpfor info on the diiferences",
          "(binary) quickly installed, bigger
           (source) takes a while but more system specific
           (git) compiling newest git source, unstable but new",
      );
  
      match wanted_version.as_str() {
          "git" => return "git".to_owned(),
          "source" => return "source".to_owned(),
          "binary" => return "binary".to_owned(),
          _ => return "Yo WTF did you to actually get this output? Absoloute chad".to_owned(),
      }
}