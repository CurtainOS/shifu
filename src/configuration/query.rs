use toml::Value;
pub fn handle_query(query: Vec<String>, config: Value) -> Vec<String> {
    let vec1 = replace_with_shortcuts(query, config);
    replace_duplicates(vec1)

}

fn replace_with_shortcuts(query: Vec<String>, config: Value) -> Vec<String> {
    let mut return_vec = Vec::new();
    for i in query {
     if config["package"].get(&i) == None {
     return_vec.push(i);
     continue;
     } else {
        let to_append = config["package"].get(&i).unwrap().as_array().unwrap();
        let mut vec_to_append = Vec::new();
        for i in to_append {
            let i_clone = i.as_str().unwrap().to_owned();
            vec_to_append.push(i_clone.to_string());
        }
        println!("{:?}", vec_to_append);
        return_vec.extend(vec_to_append);

     }
} 
   return_vec
}

fn replace_duplicates(query: Vec<String>) -> Vec<String> {
    let mut already_seen = Vec::new();
    let mut vec_to_return = Vec::new();
    for i in query {
        if !already_seen.contains(&i) {
            already_seen.push(i.clone());
            vec_to_return.push(i);

        }
    }
    vec_to_return
}