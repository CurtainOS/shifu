use regex::Regex;

pub fn use_prefix(input_str: String, prefix: &str) -> String {

    let fixconfigure = format!("./configure --prefix={}$1", prefix);
    let fixmeson = format!("meson --prefix={}$1", prefix);

    let fixcmake = format!("-DCMAKE_INSTALL_PREFIX={}$1", prefix);

    let mesonregex = Regex::new(r"(?m)^meson --prefix=/usr(\s)").unwrap();
    let configureregex = Regex::new(r"(?m)^./configure --prefix=/usr(\s)").unwrap();

    let didcmake = &input_str.replace("-DCMAKE_INSTALL_PREFIX=/usr", fixcmake.as_str());
    let didmeson = mesonregex.replace_all(didcmake, fixmeson).to_string();
    let didgeneric = &didmeson.replace("%prefix%", prefix);
    let finalresult = configureregex
        .replace_all(didgeneric, fixconfigure)
        .to_string();
    finalresult.replace("prefix=/usr", format!("prefix={}", prefix).as_str())
}
