use regex::Regex;

pub fn destdir(file: &str, destination: &String) -> String {
    //regex and fix for make based packages
    let make_regex = Regex::new(r"(?m)make install(\s)").unwrap();
    let make_fix = format!("make DESTDIR={} install$1", destination);

    //regex and fix for ninja based packages
    let ninja_regex = Regex::new(r"(?m)ninja install(\s)").unwrap();
    let ninja_fix = format!("DESTDIR={} ninja install$1", destination);

    //regex for other packages where we let %destination% be the indicator
    let dest_regex = Regex::new(r"%destination%").unwrap();
    let dest_fix = destination;

    let applied_ninja = ninja_regex.replace_all(file, ninja_fix).to_string();
    let applied_dest = dest_regex.replace_all(&applied_ninja, dest_fix).to_string();
    let finished = make_regex.replace_all(&applied_dest, make_fix).to_string();

    finished.replace("%destdir%", format!("{}/", destination).as_str())
}
