use std::path::Path;
use std::{thread, time};
pub fn request_lock() {
    let mut i = 0;
    while i < 20 {
    if Path::new("/etc/shifu/db.lck").exists() {
        thread::sleep(time::Duration::from_secs(1));
        i += 1;
        continue; 

    } else {
        std::fs::File::create("/etc/shifu/db.lck").unwrap();
        return; 
    }}
    panic!("The file /etc/shifu/db.lck was present for 20 seconds. If you are sure that there is no other instance of shifu running this must be a mistake and you should manually remove it.")

}
pub fn remove_lock() {
    std::fs::remove_file("/etc/shifu/db.lck").unwrap();
}