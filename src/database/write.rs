use rusqlite::Connection;
use crate::structs::DatabaseEntry;
use crate::database::lock::*;
use crate::database::delete::delete_by_name;
use crate::database::read::*;
pub fn add_db_package_entry(name: String, version: String, installtype: String, description: String, depends: Option<Vec<String>>) {
    let structtoadd = DatabaseEntry {
        id: 0,
        name,
        installtype,
        version, //has to be string because some packages have wierd version names
        description,
        depends,
    };
    // let dependencies_table = "CREATE TABLE IF NOT EXISTS ".to_owned() + &structtoadd.name + "dependencies (
    //     id          INTEGER NOT NULL PRIMARY KEY,
    //     name        TEXT NOT NULL
    // )";
    // let dependents_table = "CREATE TABLE IF NOT EXISTS ".to_owned() + &structtoadd.name + "dependents (
    //     id          INTEGER NOT NULL PRIMARY KEY,
    //     name        TEXT NOT NULL
    // )";

    if std::path::Path::new("/etc/shifu/shifu.db").exists() && check_if_installed(structtoadd.name.clone()) {
        //replace entry if this is an update
        println!("1");
        delete_by_name(structtoadd.name.clone(), "installedpackages".to_string());   
        request_lock();
        println!("2");

        let db = Connection::open("/etc/shifu/shifu.db").unwrap();
        db.execute("INSERT INTO installedpackages (name, installtype, version, description) VALUES (?1, ?2, ?3, ?4)", (&structtoadd.name, &structtoadd.installtype, &structtoadd.version, &structtoadd.description)).unwrap();
        db.close().unwrap();
        remove_lock();
    } else {
        //creating an absoloute new entry
    request_lock();
    let db = Connection::open("/etc/shifu/shifu.db").unwrap();
    db.execute(
        "CREATE TABLE IF NOT EXISTS installedpackages (
            id          INTEGER NOT NULL PRIMARY KEY,
            date        TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
            name        TEXT NOT NULL,
            installtype TEXT NOT NULL,
            version     TEXT NOT NULL,
            description TEXT NOT NULL
        )",
        (), // empty list of parameters.
    ).unwrap();
    db.execute("INSERT INTO installedpackages (name, installtype, version, description) VALUES (?1, ?2, ?3, ?4)", (&structtoadd.name, &structtoadd.installtype, &structtoadd.version, &structtoadd.description)).unwrap();
    // db.execute(dependencies_table.as_str(),
    //  (),
    //  ).unwrap();
    // db.execute(dependents_table.as_str(),
    //  (),
    //  ).unwrap();
    // if structtoadd.depends != None {
    // for i in &structtoadd.depends.unwrap() {
    //     let to_execute = "INSERT INTO ".to_owned() + i + "dependents (name) VALUES (?1)";
    //     db.execute(to_execute.as_str(), (&structtoadd.name)).unwrap();
    // }}
    db.close().unwrap();
    remove_lock();
    println!("done");
}}

