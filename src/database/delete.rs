use rusqlite::Connection;
use crate::database::lock::*;

pub fn delete_by_name(name: String, table_name: String) {
    let delete_command = "DELETE FROM ".to_owned() + &table_name + " WHERE name = \'" + &name + "\'"; 
    request_lock();
    let db = Connection::open("/etc/shifu/shifu.db").unwrap();
    db.execute(&delete_command, ()).unwrap();

    remove_lock();

}