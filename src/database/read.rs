use rusqlite::Connection;
use crate::structs::DatabaseEntry;
use crate::database::lock::*;
pub fn check_if_installed(package_name: String) -> bool {

    request_lock();
    let db = Connection::open("/etc/shifu/shifu.db").unwrap();
    let mut stmt = db.prepare("SELECT id, name, installtype, version, description FROM installedpackages").unwrap();
    let person_iter = stmt.query_map([], |row| {
        Ok(DatabaseEntry {
            id: row.get(0).unwrap(),
            name: row.get(1).unwrap(),
            installtype: row.get(2).unwrap(),
            version: row.get(3).unwrap(),
            description: row.get(4).unwrap(),
            depends: None,
        })
    }).unwrap();

    for person in person_iter {
        if person.unwrap().name == package_name {
            remove_lock();
           return true
        }
    }
    remove_lock();
    false


}
#[allow(dead_code)]
pub fn get_version(package_name: String) -> Result<String, String> {
    if check_if_installed(package_name.clone()) {
        request_lock();
        let db = Connection::open("/etc/shifu/shifu.db").unwrap();
        let mut stmt = db.prepare("SELECT id, name, installtype, version, description FROM installedpackages").unwrap();
        let person_iter = stmt.query_map([], |row| {
        Ok(DatabaseEntry {
            id: row.get(0).unwrap(),
            name: row.get(1).unwrap(),
            installtype: row.get(2).unwrap(),
            version: row.get(3).unwrap(),
            description: row.get(4).unwrap(),
            depends: None,
        })
    }).unwrap();

    for person in person_iter {
        if person.as_ref().unwrap().name == package_name {
            remove_lock();
           return Ok(person.unwrap().version)
        }
    
    }
    remove_lock();
    return Ok("blah".to_string()) //This code will never ever run, but the compiler does not know

    } else {
        remove_lock();
        Err("package does not exist in db".to_string())
    }
}
pub fn list_all_installed_packages() {
    request_lock();
        let db = Connection::open("/etc/shifu/shifu.db").unwrap();
        let mut stmt = db.prepare("SELECT id, name, installtype, version, description FROM installedpackages").unwrap();
        let package_iter = stmt.query_map([], |row| {
            Ok(DatabaseEntry {
                id: row.get(0).unwrap(),
                name: row.get(1).unwrap(),
                installtype: row.get(2).unwrap(),
                version: row.get(3).unwrap(),
                description: row.get(4).unwrap(),
                depends: None,
            })
        }).unwrap();
    
        for package in package_iter {
            println!("{} {}", package.as_ref().unwrap().name, package.as_ref().unwrap().version)
            }
            remove_lock();

        }
